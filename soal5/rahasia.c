#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

// source : https://gitlab.com/0xazr/sisop-praktikum-modul-4-2023-MHFD-IT02/-/blob/main/soal5/rahasia.c
// gcc -Wall `pkg-config fuse --cflags` rahasia.c -o run `pkg-config fuse --libs` -lcrypto


// mkdir temp/

// ./run temp/



// sudo umount temp/
// atau
// fusermount -u temp/

// static const char *dirpath = "/home/key/sisop-praktikum-modul-4-2023-bj-a13/soal5/rahasia";
static const char *dirpath = "/home/vito/sisop/sisop-praktikum-modul-4-2023-bj-a13/soal5/rahasia";

struct ExtensionCount {
    char extension[10];
    int count;
};

void download_gdrive(const char *id, const char *filename);
void unzip(const char *filename);
void user_register(const char *username, const char *password);
void user_login(const char *username, const char *password);

void* xmp_init(struct fuse_conn_info *conn);
static int xmp_getattr(const char *path, struct stat *stbuf);
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi);
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
static int xmp_rename(const char *from, const char *to);

void rename_files(const char *dirPath);
void rename_folders(const char *dirPath);
void countFilesAndFolders(const char *dirPath, struct ExtensionCount *extensions, int *folderCount);
void updateExtensionCount(struct ExtensionCount *extensions, const char *extension);
void writeExtensionCountToFile(const struct ExtensionCount *extensions, int folderCount);

static struct fuse_operations xmp_oper = {
    .init = xmp_init,
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
    .read = xmp_read,
};

int *is_logged_in;

int main(int  argc, char *argv[])
{
    key_t key = 1234;
    int shmid = shmget(key, sizeof(int), IPC_CREAT | 0666);
    is_logged_in = shmat(shmid, NULL, 0);
    *is_logged_in = 0;

    umask(0);

    // Download zip
    download_gdrive("18YCFdG658SALaboVJUHQIqeamcfNY39a", "rahasia.zip");

    // Unzip
    unzip("rahasia.zip");

    pid_t pid;

    pid = fork();
    
    if (pid == -1) {
        perror("fork");
        exit(1);
    } else if (pid == 0) {
        fuse_main(argc, argv, &xmp_oper, NULL);
    } else {
        // Menu
        char username[1000], password[1000];
        int choice;

        printf("Welcome to my filesystem\n");
        printf("You need to login first\n");
        printf("WARNING: The mounted folder can't be accessed before login\n");
        
        while (1)
        {
            if (*is_logged_in == 0) {
                printf("[1] Register\n");
                printf("[2] Login\n");
                printf("Choice: ");
                scanf("%d", &choice);

                if (choice == 1) {
                    printf("Register\n");
                    printf("Username: ");
                    scanf("%s", username);
                    printf("Password: ");
                    scanf("%s", password);
                    user_register(username, password);
                } else if (choice == 2) {
                    printf("Login\n");
                    printf("Username: ");
                    scanf("%s", username);
                    printf("Password: ");
                    scanf("%s", password);
                    user_login(username, password);
                } else {
                    printf("Invalid choice\n");
                }
            } else {
                printf("Press enter to run main program\n");
                
                int c;
                while((c = getchar()) != '\n' && c != EOF);
                getchar();
                // TODO: Run main program
                rename_folders("rahasia");
                rename_files("rahasia");

                // List all files inside rahasia and its subfolder
                system("tree rahasia > result.txt");
                
                // Count all items inside rahasia and its subfolder
                const char *folderPath = "rahasia";
                
                struct ExtensionCount extensions[100];
                
                for (int i = 0; i < 100; i++) {
                    strcpy(extensions[i].extension, "");
                    extensions[i].count = 0;
                }
                int folderCount = 0;

                countFilesAndFolders(folderPath, extensions, &folderCount);
                writeExtensionCountToFile(extensions, folderCount);
                
                break;
            }
        }
    }

    return 0;
}

void download_gdrive(const char *id, const char *filename)
{
    printf("[+] Downloading \"%s\"\n", filename);
    char command[1000];
    sprintf(command, "wget -q --load-cookies /tmp/cookies.txt \"https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate '%s' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\\1\\n/p')&id=%s\" -O %s && rm -rf /tmp/cookies.txt", id, id, filename);
    system(command);
    printf("[+] Download completed\n");
}

void unzip(const char *filename)
{
    printf("[+] Unzipping \"%s\"\n", filename);
    char command[1000];
    sprintf(command, "unzip -q %s", filename);
    system(command);
    printf("[+] Unzip finished\n");
}

void user_register(const char *username, const char *password)
{
    printf("[+] Registering user \"%s\"\n", username);
    // Store username and password to file
    // Check if users.txt exists
    if (access("users.txt", F_OK) == -1) {
        // Create users.txt
        FILE *fp = fopen("users.txt", "w");
        fclose(fp);
    }

    // Check if username exists
    FILE *fp = fopen("users.txt", "r");
    char line[1000];
    while (fgets(line, sizeof(line), fp)) {
        char *token = strtok(line, ";");
        if (strcmp(token, username) == 0) {
            printf("[+] Username already exists\n");
            int c;
            while((c = getchar()) != '\n' && c != EOF);
            getchar();
            return;
        }
    }
    fclose(fp);

    // Append username and password to users.txt
    fp = fopen("users.txt", "a");
    
    // Password encryption using md5sum
    char command[1000];
    sprintf(command, "echo -n \"%s\" | md5sum | awk '{print $1}'", password);
    FILE *md5sum = popen(command, "r");
    char encrypted_password[1000];
    fgets(encrypted_password, sizeof(encrypted_password), md5sum);
    encrypted_password[strlen(encrypted_password) - 1] = '\0';
    pclose(md5sum);

    // Append username and password to users.txt
    fprintf(fp, "%s;%s\n", username, encrypted_password);
    fclose(fp);

    printf("[+] User \"%s\" registered\n", username);
    int c;
    while((c = getchar()) != '\n' && c != EOF);
    getchar();
}

void user_login(const char *username, const char *password)
{
    // Check if users.txt exists
    if (access("users.txt", F_OK) == -1) {
        // Create users.txt
        printf("[!] No user registered\n");
        int c;
        while((c = getchar()) != '\n' && c != EOF);
        getchar();
        return;
    }

    // Check if username exists
    FILE *fp = fopen("users.txt", "r");
    char line[1000];
    while (fgets(line, sizeof(line), fp)) {
        char *token = strtok(line, ";");
        if (strcmp(token, username) == 0) {
            // Check password
            token = strtok(NULL, ";");
            token[strlen(token) - 1] = '\0';
            
            char command[1000];
            sprintf(command, "echo -n \"%s\" | md5sum | awk '{print $1}'", password);
            FILE *md5sum = popen(command, "r");
            char encrypted_password[1000];
            fgets(encrypted_password, sizeof(encrypted_password), md5sum);
            encrypted_password[strlen(encrypted_password) - 1] = '\0';
            pclose(md5sum);
            
            if (strcmp(token, encrypted_password) == 0) {
                printf("[+] Login success\n");
                *is_logged_in = 1;
                int c;
                while((c = getchar()) != '\n' && c != EOF);
                getchar();
                return;
            } else {
                printf("[!] Wrong password\n");
                int c;
                while((c = getchar()) != '\n' && c != EOF);
                getchar();
                return;
            }
        }
    }
}

void* xmp_init(struct fuse_conn_info *conn)
{
    
    return NULL;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    if (*is_logged_in == 0) {
        printf("[!] You are not logged in\n");
        return -1;
    }

    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    if (*is_logged_in == 0) {
        printf("[!] You are not logged in\n");
        return -1;
    }

    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if (*is_logged_in == 0) {
        printf("[!] You are not logged in\n");
        return -1;
    }

    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to) {
    if (*is_logged_in == 0) {
        printf("[!] You are not logged in\n");
        return -1;
    }

    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    int res = rename(fpath_from, fpath_to);
    if (res == -1) return -errno;

    return 0;
}
void rename_files(const char *dirPath) {
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        printf("Error opening directory: %s\n", dirPath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char filePath[1000];
        snprintf(filePath, sizeof(filePath), "%s/%s", dirPath, entry->d_name);

        if (entry->d_type == DT_DIR) {
            rename_files(filePath);  // Recursively process subdirectories
        } else {
            // Rename the file
            char newFilePath[1000];
            char *fileName = entry->d_name;
            char *fileExtension = strrchr(fileName, '.');
            
            if (fileExtension == NULL) {
                printf("Skipping file without extension: %s\n", fileName);
                continue;
            }
            
            int fileNameLength = strlen(fileName);
            int fileExtensionLength = strlen(fileExtension);
            int newFileNameLength = fileNameLength + 6 + fileExtensionLength;
            char newFileName[newFileNameLength + 1];
            snprintf(newFileName, sizeof(newFileName), "A13_%s", fileName);
            
            snprintf(newFilePath, sizeof(newFilePath), "%s/%s", dirPath, newFileName);
            
            if (rename(filePath, newFilePath) == 0) {
                printf("Renamed file: %s\n", entry->d_name);
            } else {
                printf("Error renaming file: %s\n", entry->d_name);
            }
        }
    }

    closedir(dir);
}

void rename_folders(const char *dirPath) {
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        printf("Error opening directory: %s\n", dirPath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type != DT_DIR || strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char folderPath[1000];
        snprintf(folderPath, sizeof(folderPath), "%s/%s", dirPath, entry->d_name);

        // Rename the folder
        char newFolderPath[1000];
        snprintf(newFolderPath, sizeof(newFolderPath), "%s/%s_A13", dirPath, entry->d_name);

        if (rename(folderPath, newFolderPath) == 0) {
            printf("Renamed folder: %s\n", entry->d_name);
            // Recursively process subfolders
            rename_folders(newFolderPath);
        } else {
            printf("Error renaming folder: %s\n", entry->d_name);
        }
    }

    closedir(dir);
}

void countFilesAndFolders(const char *dirPath, struct ExtensionCount *extensions, int *folderCount) {
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        printf("Error opening directory: %s\n", dirPath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char filePath[1000];
        snprintf(filePath, sizeof(filePath), "%s/%s", dirPath, entry->d_name);

        if (entry->d_type == DT_DIR) {
            (*folderCount)++;
            countFilesAndFolders(filePath, extensions, folderCount);
        } else {
            char *fileExtension = strrchr(entry->d_name, '.');

            if (fileExtension != NULL) {
                updateExtensionCount(extensions, fileExtension + 1);
            }
        }
    }

    closedir(dir);
}

void updateExtensionCount(struct ExtensionCount *extensions, const char *extension) {
    for (int i = 0; i < 100; i++) {
        if (strcmp(extensions[i].extension, extension) == 0) {
            extensions[i].count++;
            return;
        }
        if (strlen(extensions[i].extension) == 0) {
            strcpy(extensions[i].extension, extension);
            extensions[i].count = 1;
            return;
        }
    }
}

void writeExtensionCountToFile(const struct ExtensionCount *extensions, int folderCount) {
    FILE *file = fopen("extension.txt", "w");
    if (file == NULL) {
        printf("Error creating extension.txt\n");
        return;
    }

    fprintf(file, "folder = %d\n", folderCount);

    for (int i = 0; i < 100; i++) {
        if (strlen(extensions[i].extension) == 0) {
            break;
        }
        fprintf(file, "%s = %d\n", extensions[i].extension, extensions[i].count);
    }

    fclose(file);
}