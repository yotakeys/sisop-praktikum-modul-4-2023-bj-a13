#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

void makeLog(const char *res, const char *operation, const char *desc, const char *add);
const char *src = "/home/adrian/Documents/Praktikum4/soal2/nanaxgerma/src_data";
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);
    res = lstat(newpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);

    printf("readdir %s\n", newpath);
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(newpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    printf("read %s\n", newpath);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(newpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static bool restricted(char *path) {
    bool restricted = false;
    char *copy = strdup(path);
    char *token = strtok(copy, "/");

    while (token != NULL) {
        if (strstr(token, "restricted") != NULL) {
            restricted = true;
        }
        if(strstr(token, "bypass") != NULL){
            restricted = false;
        }
        token = strtok(NULL, "/");
    }

    return restricted;
}


static int xmp_mkdir(const char *path, mode_t mode)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    // Check if the path or any parent directories are restricted
    if (restricted(newpath))
    {
        makeLog("FAILED", "MKDIR", newpath,"");
        return -EACCES; 
    }
    
    int res = mkdir(newpath, mode);

    if (res == -1)
    {
        //means failed
        makeLog("FAILED", "MKDIR", newpath,"");
        return -errno;
    }
    
    //success
    makeLog("SUCCESS", "MKDIR", newpath,"");
    return 0;
}

static int xmp_rmdir(const char *path)
{
    char newpath[1024];
    int res;
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -EACCES;
    }

    res = rmdir(newpath);
    if (res == -1){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -errno;
    }
    //make log success
    makeLog("SUCCESS", "RMDIR", newpath,"");
    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;
    char before[1024];
    strcpy(before, src);
    strcat(before, from);

    char new[1024];
    strcpy(new, src);
    strcat(new, to);

    if(restricted(before)){
        if(restricted(new)){
        makeLog("FAILED", "RENAME", before,new);
        return -EACCES;
        }
    }

    res = rename(before, new);

    if (res == -1){
        makeLog("FAILED", "RENAME", before,new);
        return -errno;
    }

    makeLog("SUCCESS", "RENAME", before,new);
    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -EACCES;
    }

    res = unlink(newpath);

    if (res == -1){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -errno;
    }
    makeLog("SUCCESS", "RMFILE", newpath,"");

    return 0;
}


static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if (restricted(newpath)) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -EACCES;
    }

    int fd = creat(newpath, mode);
    if (fd == -1) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -errno;
    }

    fi->fh = fd;

    makeLog("SUCCESS", "TOUCH", newpath, "");

    return 0;
}

void makeLog(const char *res, const char *operation, const char *desc, const char *add){
    
    time_t now;
    time(&now);
    struct tm *timestamp = localtime(&now);


FILE *logging = fopen("/home/adrian/Documents/Praktikum4/soal2/logmucatatsini.txt", "a");


char clock[20];
strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

if (logging != NULL) {

    fprintf(logging, "%s::%s::%s::adrian-", res, clock, operation);

    if (strcmp(operation, "MKDIR") == 0) {
        fprintf(logging, "Create directory %s\n", desc);
    } else if (strcmp(operation, "RENAME") == 0) {
        fprintf(logging, "Rename from %s to %s\n", desc, add);
    } else if (strcmp(operation, "RMDIR") == 0) {
        fprintf(logging, "Remove directory %s\n", desc);
    } else if (strcmp(operation, "RMFILE") == 0) {
        fprintf(logging, "Remove file %s\n", desc);
    }else if (strcmp(operation, "TOUCH") == 0) {
        fprintf(logging, "Created file %s\n", desc);
    }

    fclose(logging);

}
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .rename = xmp_rename,
    .create = xmp_create,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}