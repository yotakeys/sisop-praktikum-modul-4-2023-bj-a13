# Sisop Praktikum Modul 3 2023 BJ-A13

Anggota :

1. Keyisa Raihan I. S. (5025211002)
2. M. Taslam Gustino (5025211011)
3. Adrian Karuna S. (5025211019)
4. Vito Febrian Ananta (5025211224)

# Soal 1

Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran.
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

## Solusi A

```c
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
system("unzip fifa-player-stats-database.zip");
```

Download dengan menggunakan system dimana download menggunakan kaggle output download itu berupa zip. Untuk melakukan zip menggunakan system unzip dengan nama file.zip tersebut.

## Solusi B

```c
system("awk -F, '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {print $2, $3, $9, $5, $8, $4}' FIFA23_official_data.csv");
```

Untuk membaca file CSV dengan nama file yang ditentukan yaitu menggunakan awk '-F ,' digunakan untuk memisahkan kolom dengan tanda koma. '$3 < 25 && $8 > 85 && $9 != \"Manchester City\" {print $2, $3, $9, $5, $8, $4}' untuk filter kolom-kolom yang sesuai dengan yang diinginkan $3 menandakan kolom ke 3 dan seterusnya. Diakhiri oleh nama file yang ingin dibaca.

## Solusi C

```c
FROM ubuntu:22.04

ENV KAGGLE_USERNAME="gustino"
ENV KAGGLE_KEY="0882f1eaa3fe64aeea49ef58de5d0915"

RUN apt-get update
RUN apt install unzip -y
RUN apt install python3 -y
RUN apt install pip -y
RUN pip install kaggle

COPY storage /app
CMD ["./app", "storage"]
```

Code diatas merupakan isi dari docker file. Container menggunakan system ubuntu dengan menggunakan environment variable untuk tambahan menggunakan kaggle. Install semua yang diinginkan didalam container untuk dapat menjalankan executable dari storage.c yang sudah di compile diluar container. Kemudian melakukan copy executable dari storage.c kedalam container. Untuk perintah saat container dijalankan menggunakan CMD dimana didalamnya menjalankan file executable storage.

### Output

Berikut langkah-langkah beserta ouput-nya untuk menjalankan images sendiri tanpa menggunakan docker compose :
![Screenshot_from_2023-06-03_11-44-50](/uploads/bba49bb703b694d096ca0acb61c5f171/Screenshot_from_2023-06-03_11-44-50.png)

![Screenshot_from_2023-06-03_11-45-06](/uploads/dd886e2e9070726b042740b0fd2109a5/Screenshot_from_2023-06-03_11-45-06.png)

## Solusi D

Untuk melakukan push Docker Image kedalam Docker hub dilakukan login terlebih dahulu pada terminal. Kemudian melakukan tag docker image yang diinginkan dengan nama yang sesuai. Terakhir melakukan push dengan perintah :

```
sudo docker push <nama_image>:<tag>
```

### Ouput

Berikut langkah-langkah beserta output-nya untuk melakukan push ke docker hub
![Screenshot_from_2023-06-03_11-45-36](/uploads/2d128fe41447092f9cd884cfd7c8e604/Screenshot_from_2023-06-03_11-45-36.png)

![Screenshot_from_2023-06-03_11-46-00](/uploads/0729d0bd9b5ee65ee8ca8d1e2e258dd2/Screenshot_from_2023-06-03_11-46-00.png)

## Solusi E

```
version: "3"

services:
  storage-app:
    image: storage
    deploy:
      replicas: 5
    ports:
      - 8080-8084:80
```

Code diatas yaitu konfigurasi yang berada pada docker-compose.yaml dengan memberikan nama untuk container yaitu storage-app dengan image yang sudah di build sebelumnya dari soal C dengan nama 'storage'. Kemudian dilakukan replicas 5 untuk membuat 5x docker build di run untuk menjadi container dengan port 8080-8084 untuk setiap container-nya.
Code diatas dijalankan di dalam folder Barcelona dan Napoli yang dibuat manual dengan command :

```c
sudo docker compose up
```

### Output

Berikut langkah-langkah beserta output-nya untuk melakukan docker compose pada folder Barcelona, untuk folder Napoli caranya sama

![Screenshot_from_2023-06-03_11-46-38](/uploads/b9ffb4c1b9401cfc67140dc1b76bbef6/Screenshot_from_2023-06-03_11-46-38.png)

![Screenshot_from_2023-06-03_11-46-44](/uploads/fd27224081c7a6eef9c9d38d38a073bb/Screenshot_from_2023-06-03_11-46-44.png)

![Screenshot_from_2023-06-03_11-46-51](/uploads/2b1c58300138c3b96c011c53c31c9590/Screenshot_from_2023-06-03_11-46-51.png)

# Soal 2

Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:

- Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
- Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
  Contoh:
  /jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.

### Case invalid untuk bypass:

/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt

### Case valid untuk bypass:

/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

- Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. Untuk mengujinya, maka lakukan hal-hal berikut:

- Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
  Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.

- Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.

- Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus

- Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa.
  Adapun format logging yang harus kamu buat adalah sebagai berikut.
  [STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
  Dengan keterangan sebagai berikut.
  STATUS: SUCCESS atau FAILED.
  dd: 2 digit tanggal.
  MM: 2 digit bulan.
  yyyy: 4 digit tahun.
  HH: 24-hour clock hour, with a leading 0 (e.g. 22).
  mm: 2 digit menit.
  ss: 2 digit detik.
  CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
  DESC: keterangan action, yaitu:
  [User]-Create directory x.
  [User]-Rename from x to y.
  [User]-Remove directory x.
  [User]-Remove file x.

Contoh:
SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y

## Solution:

```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

void makeLog(const char *res, const char *operation, const char *desc, const char *add);
const char *src = "/home/adrian/Documents/Praktikum4/soal2/nanaxgerma/src_data";
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);
    res = lstat(newpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);

    printf("readdir %s\n", newpath);
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(newpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    printf("read %s\n", newpath);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(newpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}

static bool restricted(char *path) {
    bool restricted = false;
    char *copy = strdup(path);
    char *token = strtok(copy, "/");

    while (token != NULL) {
        if (strstr(token, "restricted") != NULL) {
            restricted = true;
        }
        if(strstr(token, "bypass") != NULL){
            restricted = false;
        }
        token = strtok(NULL, "/");
    }

    return restricted;
}


static int xmp_mkdir(const char *path, mode_t mode)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    // Check if the path or any parent directories are restricted
    if (restricted(newpath))
    {
        makeLog("FAILED", "MKDIR", newpath,"");
        return -EACCES;
    }

    int res = mkdir(newpath, mode);

    if (res == -1)
    {
        //means failed
        makeLog("FAILED", "MKDIR", newpath,"");
        return -errno;
    }

    //success
    makeLog("SUCCESS", "MKDIR", newpath,"");
    return 0;
}

static int xmp_rmdir(const char *path)
{
    char newpath[1024];
    int res;
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -EACCES;
    }

    res = rmdir(newpath);
    if (res == -1){
        makeLog("FAILED", "RMDIR", newpath,"");
        return -errno;
    }
    //make log success
    makeLog("SUCCESS", "RMDIR", newpath,"");
    return 0;
}

static int xmp_rename(const char *from, const char *to)
{
    int res;
    char before[1024];
    strcpy(before, src);
    strcat(before, from);

    char new[1024];
    strcpy(new, src);
    strcat(new, to);

    if(restricted(before)){
        if(restricted(new)){
        makeLog("FAILED", "RENAME", before,new);
        return -EACCES;
        }
    }

    res = rename(before, new);

    if (res == -1){
        makeLog("FAILED", "RENAME", before,new);
        return -errno;
    }

    makeLog("SUCCESS", "RENAME", before,new);
    return 0;
}

static int xmp_unlink(const char *path)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if(restricted(newpath)){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -EACCES;
    }

    res = unlink(newpath);

    if (res == -1){
        makeLog("FAILED", "RMFILE", newpath,"");
        return -errno;
    }
    makeLog("SUCCESS", "RMFILE", newpath,"");

    return 0;
}


static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    if (restricted(newpath)) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -EACCES;
    }

    int fd = creat(newpath, mode);
    if (fd == -1) {
        makeLog("FAILED", "TOUCH", newpath, "");
        return -errno;
    }

    fi->fh = fd;

    makeLog("SUCCESS", "TOUCH", newpath, "");

    return 0;
}

void makeLog(const char *res, const char *operation, const char *desc, const char *add){

    time_t now;
    time(&now);
    struct tm *timestamp = localtime(&now);


FILE *logging = fopen("/home/adrian/Documents/Praktikum4/soal2/logmucatatsini.txt", "a");


char clock[20];
strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

if (logging != NULL) {

    fprintf(logging, "%s::%s::%s::adrian-", res, clock, operation);

    if (strcmp(operation, "MKDIR") == 0) {
        fprintf(logging, "Create directory %s\n", desc);
    } else if (strcmp(operation, "RENAME") == 0) {
        fprintf(logging, "Rename from %s to %s\n", desc, add);
    } else if (strcmp(operation, "RMDIR") == 0) {
        fprintf(logging, "Remove directory %s\n", desc);
    } else if (strcmp(operation, "RMFILE") == 0) {
        fprintf(logging, "Remove file %s\n", desc);
    }else if (strcmp(operation, "TOUCH") == 0) {
        fprintf(logging, "Created file %s\n", desc);
    }

    fclose(logging);

}
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
    .rename = xmp_rename,
    .create = xmp_create,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

untuk kode pada bagian getattr, readdir, dan xmp_read saya menggunakan kode yang sama dengan modul, tetapi perbedaanya adalah untuk path source saya arahkan pada folder nanaxgerma yang ada di komputer saya.

```c

const char *src = "/home/adrian/Documents/Praktikum4/soal2/nanaxgerma/src_data";
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);
    res = lstat(newpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);
    printf("getattr %s\n", newpath);

    printf("readdir %s\n", newpath);
    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(newpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        res = (filler(buf, de->d_name, &st, 0));
        if (res != 0)
            break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char newpath[1024];
    strcpy(newpath, src);
    strcat(newpath, path);

    printf("read %s\n", newpath);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(newpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}
```

kode ini akan membaca (read) dari folder src nanaxgerma

kemudian kita diminta untuk mengecek nama dari directory/file yang ada, apabila terdapat kata "restricted" maka folder/file tersebut tidak dapat kita kita edit/utak-atik. namun apabila terdapat kata "bypass" meskipun pada judul yang ada "restricted" maka file/folder tersebut dapat di edit/utak-atik.

berikut adalah code untuk mengecek nama directory

```c
static bool restricted(char *path) {
    bool restricted = false;
    char *copy = strdup(path);
    char *token = strtok(copy, "/");

    while (token != NULL) {
        if (strstr(token, "restricted") != NULL) {
            restricted = true;
        }
        if(strstr(token, "bypass") != NULL){
            restricted = false;
        }
        token = strtok(NULL, "/");
    }

    return restricted;
}
```

Cara kerja dari code ini adalah, kita memecah path seperti "root/user/home/Document/praktikum4/nanaxgerma/dest_data" dengan batas "/", jadi token akan berisikan seperti root, user, home, Document, Praktikum4, kemudian digunakan fungsi strstr dari library untuk mengecek token tersebut apakah terdapat kata "restricted" atau "bypass".

Kemudian, diminta juga untuk membuat log dari eksekusi/proses yang telah dilakukan dengan format
[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]

disini digunakan library time untuk waktunya, dan string strcmp untuk mengecek jenis operation dari argumen fungsi.

```c

void makeLog(const char *res, const char *operation, const char *desc, const char *add){

    time_t now;
    time(&now);
    struct tm *timestamp = localtime(&now);


FILE *logging = fopen("/home/adrian/Documents/Praktikum4/soal2/logmucatatsini.txt", "a");


char clock[20];
strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

if (logging != NULL) {

    fprintf(logging, "%s::%s::%s::adrian-", res, clock, operation);

    if (strcmp(operation, "MKDIR") == 0) {
        fprintf(logging, "Create directory %s\n", desc);
    } else if (strcmp(operation, "RENAME") == 0) {
        fprintf(logging, "Rename from %s to %s\n", desc, add);
    } else if (strcmp(operation, "RMDIR") == 0) {
        fprintf(logging, "Remove directory %s\n", desc);
    } else if (strcmp(operation, "RMFILE") == 0) {
        fprintf(logging, "Remove file %s\n", desc);
    }else if (strcmp(operation, "TOUCH") == 0) {
        fprintf(logging, "Created file %s\n", desc);
    }

    fclose(logging);

}
}
```

```c
    time_t now;
    time(&now);
    struct tm *timestamp = localtime(&now);


FILE *logging = fopen("/home/adrian/Documents/Praktikum4/soal2/logmucatatsini.txt", "a");


char clock[20];
strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

```

strftime adalah fungsi untuk mengatur format waktu dengan aturan yang telah ditentukan seperti %d, %m, %Y, dkk.

lalu untuk fopen, adalah fungsi untuk open file dengan mode a, yaitu untuk append, apabila file belum ada maka akan dibuat terlebih dahulu.

lalu fprintf timestamp dari operasi fuse, emudian tinggal cek operasi nya dengan strcmp dari argumen fungsi, dan kita gunakan fprintf untuk jenis operasi.

pemanggilan fungsi pembuatan log terdapat di setiap fuse operasi, seeprti mkdir, rmdir, dll
contohnya :

```c
static int xmp_rename(const char *from, const char *to)
{
    int res;
    char before[1024];
    strcpy(before, src);
    strcat(before, from);

    char new[1024];
    strcpy(new, src);
    strcat(new, to);

    if(restricted(before)){
        if(restricted(new)){
        makeLog("FAILED", "RENAME", before,new);
        return -EACCES;
        }
    }

    res = rename(before, new);

    if (res == -1){
        makeLog("FAILED", "RENAME", before,new);
        return -errno;
    }

    makeLog("SUCCESS", "RENAME", before,new);
    return 0;
}
```

untuk fungsi rename ini, kita cek apakah kedua nama directory mengandung restricted maka akan dibuat log fail
dengan

```c
makeLog("FAILED", "RENAME", before,new);
```

dimana failed adalah status, rename adalah operasinya, before = path, after = new path (khusus rename after).


hasil output : 
![image](/uploads/a98e7dfda982094d6287f0799d30c1a5/image.png)

dapat dilihat apabila dilakukan proses/operasi yang tidak diizinkan maka akan ada permission denied, selain itu juga akan muncul pada log apabila suatu proses/operasi yang dilakukan berhasil/gagal



# Soal 5

(Revisi)

Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi. Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.

- Pertama-tama, kita perlu membuat docker container sesuai deskripsi soal yaitu dengan base image Ubuntu. Kita dapat membuatnya dengan cara memberikan command

```c
docker compose up -d
```

- Perintah tersebut juga sekaligus melakukan mounting ke /usr/share di dalam docker container yang telah dibuat.
  Berikut isi Dockerfile:

```c
FROM ubuntu:20.04

```

Berikut isi docker-compose.yml:

```c
version: "3"
services:
  container_rahasia:
    image: rahasia_di_docker_a13
    build:
      context: .
      dockerfile: Dockerfile
    command: tail -f /dev/null
    volumes:
      - /home/vito/sisop/modul4soal5/temp:/usr/share/

```

- Lalu, kita perlu mendownload file zip lalu men unzip didalam directory kita

```c
void download_gdrive(const char *id, const char *filename)
{
    printf("[+] Downloading \"%s\"\n", filename);
    char command[1000];
    sprintf(command, "wget -q --load-cookies /tmp/cookies.txt \"https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate '%s' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\\1\\n/p')&id=%s\" -O %s && rm -rf /tmp/cookies.txt", id, id, filename);
    system(command);
    printf("[+] Download completed\n");
}

void unzip(const char *filename)
{
    printf("[+] Unzipping \"%s\"\n", filename);
    char command[1000];
    sprintf(command, "unzip -q %s", filename);
    system(command);
    printf("[+] Unzip finished\n");
}
```

- Setelah mengunzip, kita akan membuat proses baru untuk meng mount folder rahasia ke lokasi yang kita ingin tuju, untuk kode mount nya seperti yang ad pada modul

```c
    pid_t pid;

    pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(1);
    } else if (pid == 0) {
        fuse_main(argc, argv, &xmp_oper, NULL);
    }

```

- Setelah di mount, proses utama akan mewajibkan user untuk login terlebih dahulu. jiak user belum melakukan login maka user akan mendapatkan permission error ketika akan mengakses folder hasil mount. User dapat melakukan register dan login dengan datanya akan disimpan pada file users.txt

```c
 if (*is_logged_in == 0) {
                printf("[1] Register\n");
                printf("[2] Login\n");
                printf("Choice: ");
                scanf("%d", &choice);

                if (choice == 1) {
                    printf("Register\n");
                    printf("Username: ");
                    scanf("%s", username);
                    printf("Password: ");
                    scanf("%s", password);
                    user_register(username, password);
                } else if (choice == 2) {
                    printf("Login\n");
                    printf("Username: ");
                    scanf("%s", username);
                    printf("Password: ");
                    scanf("%s", password);
                    user_login(username, password);
                } else {
                    printf("Invalid choice\n");
                }
            }
```

dan untuk fungsi login/registernya

```c
void user_register(const char *username, const char *password)
{
    printf("[+] Registering user \"%s\"\n", username);
    // Store username and password to file
    // Check if users.txt exists
    if (access("users.txt", F_OK) == -1) {
        // Create users.txt
        FILE *fp = fopen("users.txt", "w");
        fclose(fp);
    }

    // Check if username exists
    FILE *fp = fopen("users.txt", "r");
    char line[1000];
    while (fgets(line, sizeof(line), fp)) {
        char *token = strtok(line, ";");
        if (strcmp(token, username) == 0) {
            printf("[+] Username already exists\n");
            int c;
            while((c = getchar()) != '\n' && c != EOF);
            getchar();
            return;
        }
    }
    fclose(fp);

    // Append username and password to users.txt
    fp = fopen("users.txt", "a");

    // Password encryption using md5sum
    char command[1000];
    sprintf(command, "echo -n \"%s\" | md5sum | awk '{print $1}'", password);
    FILE *md5sum = popen(command, "r");
    char encrypted_password[1000];
    fgets(encrypted_password, sizeof(encrypted_password), md5sum);
    encrypted_password[strlen(encrypted_password) - 1] = '\0';
    pclose(md5sum);

    // Append username and password to users.txt
    fprintf(fp, "%s;%s\n", username, encrypted_password);
    fclose(fp);

    printf("[+] User \"%s\" registered\n", username);
    int c;
    while((c = getchar()) != '\n' && c != EOF);
    getchar();
}

void user_login(const char *username, const char *password)
{
    // Check if users.txt exists
    if (access("users.txt", F_OK) == -1) {
        // Create users.txt
        printf("[!] No user registered\n");
        int c;
        while((c = getchar()) != '\n' && c != EOF);
        getchar();
        return;
    }

    // Check if username exists
    FILE *fp = fopen("users.txt", "r");
    char line[1000];
    while (fgets(line, sizeof(line), fp)) {
        char *token = strtok(line, ";");
        if (strcmp(token, username) == 0) {
            // Check password
            token = strtok(NULL, ";");
            token[strlen(token) - 1] = '\0';

            char command[1000];
            sprintf(command, "echo -n \"%s\" | md5sum | awk '{print $1}'", password);
            FILE *md5sum = popen(command, "r");
            char encrypted_password[1000];
            fgets(encrypted_password, sizeof(encrypted_password), md5sum);
            encrypted_password[strlen(encrypted_password) - 1] = '\0';
            pclose(md5sum);

            if (strcmp(token, encrypted_password) == 0) {
                printf("[+] Login success\n");
                *is_logged_in = 1;
                int c;
                while((c = getchar()) != '\n' && c != EOF);
                getchar();
                return;
            } else {
                printf("[!] Wrong password\n");
                int c;
                while((c = getchar()) != '\n' && c != EOF);
                getchar();
                return;
            }
        }
    }
}
```

- Setelah user melakukan login, maka file mounted dapat dibuka. setelah itulah baru dapat dijalankan program utama untuk mengrename file dengan tambahan identitas kelompok lalu menghitung ekstension dan juga mengahsilkan tree dan menyimpanya pada file result.txt untuk tree, extension.txt untuk ekstensi file dan jumlahnya. Uuntuk rename kita akan memanggil fungsi rename folder terlebih dahulu, baru file

```c
rename_folders("rahasia");
rename_files("rahasia");
```

dengan fungsi sebagai berikut, kita hanya perlu mentraverse direktori untuk mencar semua folder/file, lalu menggunakan fungsi rename() untuk merenamenya:

```c
void rename_files(const char *dirPath) {
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        printf("Error opening directory: %s\n", dirPath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char filePath[1000];
        snprintf(filePath, sizeof(filePath), "%s/%s", dirPath, entry->d_name);

        if (entry->d_type == DT_DIR) {
            rename_files(filePath);  // Recursively process subdirectories
        } else {
            // Rename the file
            char newFilePath[1000];
            char *fileName = entry->d_name;
            char *fileExtension = strrchr(fileName, '.');

            if (fileExtension == NULL) {
                printf("Skipping file without extension: %s\n", fileName);
                continue;
            }

            int fileNameLength = strlen(fileName);
            int fileExtensionLength = strlen(fileExtension);
            int newFileNameLength = fileNameLength + 6 + fileExtensionLength;
            char newFileName[newFileNameLength + 1];
            snprintf(newFileName, sizeof(newFileName), "A13_%s", fileName);

            snprintf(newFilePath, sizeof(newFilePath), "%s/%s", dirPath, newFileName);

            if (rename(filePath, newFilePath) == 0) {
                printf("Renamed file: %s\n", entry->d_name);
            } else {
                printf("Error renaming file: %s\n", entry->d_name);
            }
        }
    }

    closedir(dir);
}

void rename_folders(const char *dirPath) {
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        printf("Error opening directory: %s\n", dirPath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type != DT_DIR || strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char folderPath[1000];
        snprintf(folderPath, sizeof(folderPath), "%s/%s", dirPath, entry->d_name);

        // Rename the folder
        char newFolderPath[1000];
        snprintf(newFolderPath, sizeof(newFolderPath), "%s/%s_A13", dirPath, entry->d_name);

        if (rename(folderPath, newFolderPath) == 0) {
            printf("Renamed folder: %s\n", entry->d_name);
            // Recursively process subfolders
            rename_folders(newFolderPath);
        } else {
            printf("Error renaming folder: %s\n", entry->d_name);
        }
    }

    closedir(dir);
}
```

- Selanjutnya kita perlu membuat file result.txt yang berisi folder dan file dalam direktori rahasia dalam bentuk tree, kita hanya akan menggunakan fungsi tree lalu meng writenya ke file yg diinginkan

```c
// List all files inside rahasia and its subfolder
system("tree rahasia > result.txt");
```

- Terakhir, kita perlu mencari semua ekstensi dan folder yg ada, lalu menghitung jumlahnya

```c
const char *folderPath = "rahasia";

struct ExtensionCount extensions[100];

for (int i = 0; i < 100; i++) {
        strcpy(extensions[i].extension, "");
        extensions[i].count = 0;
    }
    int folderCount = 0;

countFilesAndFolders(folderPath, extensions, &folderCount);
writeExtensionCountToFile(extensions, folderCount);

```

kita perlu mentraferse direktori untuk jika bertemu sebuah direktory, maka folder_count akan bertambah, lalu jika bertemu dengan sebuah file, maka untuk ekstensi tersebut, jumlah file nya akan ditambah 1

```c
void countFilesAndFolders(const char *dirPath, struct ExtensionCount *extensions, int *folderCount) {
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        printf("Error opening directory: %s\n", dirPath);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char filePath[1000];
        snprintf(filePath, sizeof(filePath), "%s/%s", dirPath, entry->d_name);

        if (entry->d_type == DT_DIR) {
            (*folderCount)++;
            countFilesAndFolders(filePath, extensions, folderCount);
        } else {
            char *fileExtension = strrchr(entry->d_name, '.');

            if (fileExtension != NULL) {
                updateExtensionCount(extensions, fileExtension + 1);
            }
        }
    }

    closedir(dir);
}

void updateExtensionCount(struct ExtensionCount *extensions, const char *extension) {
    for (int i = 0; i < 100; i++) {
        if (strcmp(extensions[i].extension, extension) == 0) {
            extensions[i].count++;
            return;
        }
        if (strlen(extensions[i].extension) == 0) {
            strcpy(extensions[i].extension, extension);
            extensions[i].count = 1;
            return;
        }
    }
}
```

- Terakhir ktia hanya perlu menuliskan array ekstension dan jumlahnya tadi ke file

```c
void writeExtensionCountToFile(const struct ExtensionCount *extensions, int folderCount) {
    FILE *file = fopen("extension.txt", "w");
    if (file == NULL) {
        printf("Error creating extension.txt\n");
        return;
    }

    fprintf(file, "folder = %d\n", folderCount);

    for (int i = 0; i < 100; i++) {
        if (strlen(extensions[i].extension) == 0) {
            break;
        }
        fprintf(file, "%s = %d\n", extensions[i].extension, extensions[i].count);
    }

    fclose(file);
}
```

Output :
![image](/uploads/20afc73b859bdca5fede8612761e4ba7/image.png)
![image](/uploads/777125a5a5abd2b7ee6109b66b2c700d/image.png)
Tree

```
rahasia
├── 80T86jn7xh_A13
│   ├── A13_elaine-casap-82gJggDId-U-unsplash.jpg
│   └── A13_tobias-tullius-3BcSyNMJ2WM-unsplash.jpg
├── A13_b.pdf
├── A13_carles-rabada-kMde0v9tYYM-unsplash.jpg
├── A13_File-49ynD.txt
├── A13_File-Oadk3.txt
├── A13_File-PdCim.txt
├── A13_funny-eastern-short-music-vlog-background-hip-hop-beat-29-sec-148905.mp3
├── A13_mygamer.gif
├── A13_png-transparent-construction-equipment-illustrations-heavy-machinery-architectural-engineering-truck-vehicle-construction-sites-icon-material-free-free-logo-design-template-building-text-thumbnail.png
├── AG5zGHsasr_A13
│   └── A13_File-iVVm9.txt
├── dU87gUYIBN_A13
│   ├── A13_cinematic-logo-strong-and-wild-142807.mp3
│   ├── A13_clapping-opener-rhythmic-original-dynamic-promo-114789.mp3
│   ├── A13_mohamed-ahsan-cjkgrFt_6bE-unsplash.jpg
│   ├── A13_motivational-corporate-short-110681.mp3
│   ├── A13_png-transparent-free-download-icon-cloud-down-computer-system-blue-computer-blue-laptop-thumbnail.png
│   └── A13_sergey-zhesterev-lutEBBTpkeE-unsplash.jpg
├── Dyy2q9B0Ax_A13
│   ├── A13_File-CxD3o.txt
│   └── A13_patrick-hendry-jS0ysot7MwE-unsplash.jpg
├── FH2CQo5o8z_A13
│   ├── A13_File-dUnQq.txt
│   ├── A13_File-QDJhp.txt
│   ├── A13_File-uuXxv.txt
│   ├── A13_png-transparent-zombie-download-with-transparent-background-fantasy-free-thumbnail.png
│   └── A13_randall-ruiz-LVnJlyfa7Zk-unsplash.jpg
├── gGJ5r0Df82_A13
│   ├── A13_andre-tan-3SK2x-kbMSI-unsplash.jpg
│   ├── A13_andrii-ganzevych-IVlVHXgvi0Y-unsplash.jpg
│   ├── A13_carles-rabada-L7J0q5N7LjQ-unsplash.jpg
│   ├── A13_File-aBxnM.txt
│   ├── A13_File-EBdiV.txt
│   ├── A13_File-OlZ8w.txt
│   └── A13_File-XZJco.txt
├── IKJHSAU9731289dsa_A13
│   └── A13_png-transparent-mobile-phone-repair-material-mobile-phone-repair-phone-iphone-thumbnail.png
├── IzvxA5vw2U_A13
│   ├── A13_kris-mikael-krister-aGihPIbrtVE-unsplash.jpg
│   ├── A13_png-transparent-video-production-freemake-video-er-video-icon-free-angle-text-rectangle-thumbnail.png
│   └── A13_the-devilx27s-entry-143138.mp3
├── m4xOZ1lmux_A13
│   ├── A13_bharat-patil-_kPuQcU8C-A-unsplash.jpg
│   ├── A13_File-4qjlL.txt
│   ├── A13_File-uNOZN.txt
│   ├── A13_jon-tyson-Fz2BWfLDvGI-unsplash.jpg
│   └── A13_png-transparent-yellow-fire-fire-flame-fire-photography-orange-flame-thumbnail.png
├── mBFLP3XQMv_A13
│   ├── A13_epicaly-short-113909.mp3
│   ├── A13_File-6tNX4.txt
│   ├── A13_jcob-nasyr-uGPBqF1Yls0-unsplash.jpg
│   ├── A13_j.docx
│   ├── A13_ramona-flwrs-uJjhnN2WQJs-unsplash.jpg
│   ├── A13_redd-f-t8ts5bNQyWo-unsplash.jpg
│   └── A13_sami-takarautio-JiqalEW6Ml0-unsplash.jpg
├── nkSVqqpV7o_A13
│   ├── A13_c.pdf
│   ├── A13_d.pdf
│   ├── A13_epic-sport-clap-ampampamp-loop-main-9900.mp3
│   ├── A13_File-7Yf0S.txt
│   ├── A13_File-p6ZH2.txt
│   ├── A13_File-xBUoT.txt
│   ├── A13_k-on-yui-hirasawa.gif
│   ├── A13_matthew-feeney-L35x5fU07hY-unsplash.jpg
│   ├── A13_simple-logo-149190.mp3
│   ├── A13_thomas-owen-Y8vui_5mdto-unsplash.jpg
│   └── A13_upbeat-future-bass-opener-136069.mp3
├── Pp9LXs5FkR_A13
│   ├── A13_clapping-music-for-typographic-video-version-2-112975.mp3
│   ├── A13_File-UWKAJ.txt
│   ├── A13_File-X8lyf.txt
│   ├── A13_honkai-impact3rd-cute.gif
│   └── A13_wexor-tmg-L-2p8fapOA8-unsplash.jpg
├── SaEixBbm6y_A13
│   ├── A13_g.pdf
│   ├── A13_haikyuu-anime.gif
│   └── A13_h.pdf
├── uuXVhCbP3X_A13
│   ├── A13_a.pdf
│   ├── A13_e.pdf
│   ├── A13_epic-background-music-for-video-dramatic-hip-hop-24-seconds-149629.mp3
│   ├── A13_png-transparent-blue-dna-illustration-dna-3d-rendering-3d-computer-graphics-free-to-pull-dna-material-3d-computer-graphics-free-logo-design-template-photography-thumbnail.png
│   └── A13_png-transparent-tableware-plate-white-ceramic-plate-platter-dishware-download-with-transparent-background-thumbnail.png
├── VOpdACXtF3_A13
│   ├── A13_heart-ritsu-tainaka.gif
│   ├── A13_png-transparent-moon-moon-image-file-formats-nature-moon-png-thumbnail.png
│   └── A13_png-transparent-snowflake-light-snowflake-free-glass-free-logo-design-template-symmetry-thumbnail.png
├── vZAZecJwKF_A13
│   ├── A13_i.docx
│   ├── A13_png-transparent-kiwifruit-kiwi-free-fruit-kiwi-s-watercolor-painting-image-file-formats-food-thumbnail.png
│   ├── A13_png-transparent-sun-the-sun-sunscreen-light-sphere-sun-image-file-formats-orange-sphere-thumbnail.png
│   └── A13_tango-background-hip-hop-music-for-video-29-seconds-149879.mp3
├── xP4UcxRZE5_A13
│   ├── A13_energetic-background-reggaeton-short-music-27-sec-fun-vlog-music-149384.mp3
│   ├── A13_f.pdf
│   ├── A13_fun-background-hip-hop-short-music-27-sec-energetic-vlog-music-148916.mp3
│   ├── A13_kiss-hug.gif
│   ├── A13_png-transparent-pearl-material-sphere-pearl-balloon-download-with-transparent-background-free-thumbnail.png
│   ├── A13_png-transparent-yellow-fireworks-illustration-fireworks-pyrotechnics-free-to-pull-the-material-fireworks-s-free-logo-design-template-holidays-poster-thumbnail.png
│   └── A13_sirin-honkai-impact.gif
└── ZlzqBmSYbE_A13
    ├── A13_File-nENTg.txt
    ├── A13_File-NWnoK.txt
    ├── A13_File-xnlpH.txt
    ├── A13_png-transparent-bubble-illustration-water-drop-rain-drops-angle-white-text-thumbnail.png
    └── A13_png-transparent-golden-pistol-guns-pistol-free-download-pistol-firearms-thumbnail.png

18 directories, 90 files
```

Ekstension :

```
folder = 18
gif = 7
png = 16
txt = 23
docx = 2
mp3 = 14
pdf = 8
jpg = 20

```
